#!/usr/bin/env python

import click
from docx import Document
from openpyxl import load_workbook

@click.command()
@click.option('-s', '--source', prompt=True, type=click.Path(exists=True, dir_okay=False), nargs=1, help='Source file with placeholders')
@click.option('-r', '--replacements', prompt=True, type=click.Path(exists=True, dir_okay=False), nargs=1, help='Placeholder file with replacements')
@click.argument('target', type=click.Path(dir_okay=False), nargs=1)
def cli(source, replacements, target):
    """
    Parse source file and replace placeholders with replacements from replacements file.
    Write to target destination.
    """
    repl = read_replacements(replacements)
    replace_doc(source, target, repl)

def read_replacements(file_replacements):
    wb = load_workbook(filename=file_replacements, read_only=True, data_only=True)
    sheet = wb[wb.sheetnames[0]]
    idx = 2;
    cell = sheet['A{}'.format(idx)]
    results = {}
    while (cell.value):
        cond_cell = sheet['C{}'.format(idx)]
        if cond_cell.value == True:
            results[cell.value] = sheet['D{}'.format(idx)].value
        idx = idx + 1
        cell = sheet['A{}'.format(idx)]
    return results

def replace_doc(file_source, file_target, replacements):
    document = Document(file_source)
    for para in document.paragraphs:
        for key in replacements.keys():
            ph = '[{}]'.format(key)
            idx = para.text.find(ph)
            if idx > -1:
                para.text = para.text.replace(ph, replacements[key])
    document.save(file_target)

if __name__ == '__main__':
    cli()
